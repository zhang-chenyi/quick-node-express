const {
  getLogPageList,
  removeLog,
  batchRemoveLog,
} = require("../services/logServices");

class LogController {
  constructor() {}
  getPageList(query) {
    const { logType, current, size, startTime, endTime } = query;
    return getLogPageList(
      logType,
      current,
      size,
      startTime,
      endTime
    );
  }
  remove(body) {
    const { id } = body;
    return removeLog(id);
  }
  batchRemove(body) {
    const { ids } = body;
    return batchRemoveLog(ids);
  }
}
module.exports = new LogController();
