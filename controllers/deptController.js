const {
  getDeptListByPId,
  getDeptList,
  addDept,
  updateDept,
  deleteDept,
} = require("../services/deptServices");
class DeptController {
  constructor() {}
  getListByPId(body) {
    const {pId}=body
       return getDeptListByPId(pId)
  }
  getList() {
       return getDeptList()
  }
  add(body) {
    const dept = body;
    return addDept(dept);
  }
  update(body) {
    const dept = body;
    return updateDept(dept);
  }
  remove(body) {
    const { id } = body;
    return deleteDept(id);
  }
}
module.exports = new DeptController();
