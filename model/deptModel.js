const db = require("../config/dbPool");
const { getDate } = require("../utils/index");

class DeptModel {
  selectWhere(pId) {
    const sql = "select * from sys_depts where p_id=?";
    const sqlArr = [pId];
    return db.queryAsync(sql, sqlArr);
  }
  select() {
    const sql = "select * from sys_depts";
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  insert(dept) {
    dept.create_time = getDate();
    const sql =
      "insert into sys_depts(dept_id,name,p_id) values(?,?,?)";
    const sqlArr = [
      dept.deptId,
      dept.name,
      dept.pId, 
    ];
    return db.queryAsync(sql, sqlArr);
  }
  update(dept) {
    const sql =
      "update sys_depts set name=?,p_id=? where id=?";
    const sqlArr = [
      dept.name,
      dept.pId,
      dept.id,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  remove(id) {
    const sql = "delete from sys_depts where id=?";
    const sqlArr = [id];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new DeptModel();
