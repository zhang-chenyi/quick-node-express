const express = require("express");
const router = express.Router();
const {
  getListByPId,
  getList,
  add,
  update,
  remove,
} = require("../controllers/deptController");

router.get("/api/dept/getListByPId", (req, res) => {
  const { query } = req;
  getListByPId(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取部门列表失败",
        status: 1,
      });
      next(err);
    });
});
router.get("/api/dept/getList", (req, res) => {
  getList()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取部门列表失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/dept/add", (req, res) => {
  const { body } = req;
  add(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "添加部门失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/dept/update", (req, res) => {
  const { body } = req;
  update(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "修改部门失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/dept/delete", (req, res) => {
  const { body } = req;
  remove(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "删除部门失败",
        status: 1,
      });
      next(err);
    });
});
module.exports = router;
