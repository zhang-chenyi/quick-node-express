const express = require("express");
const router = express.Router();
const {
  getPermission,
  getPageList,
  getList,
  getDetail,
  getInfo,
  add,
  update,
  remove,
  batchRemove,
  updatePassword,
  resetPassword,
  enabled,
  disable,
} = require("../controllers/userController");

router.get("/api/user/getPermission", (req, res, next) => {
  const { query } = req;
  getPermission(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取权限失败",
        status: 1,
      });
      next(err);
    });
});
router.get("/api/user/getPageList", (req, res, next) => {
  const { query } = req;
  getPageList(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取用户分页列表失败",
        status: 1,
      });
      next(err);
    });
});
router.get("/api/user/getList", (req, res, next) => {
  const { query } = req;
  getList(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取用户列表失败",
        status: 1,
      });
      next(err);
    });
});
router.get("/api/user/getDetail", (req, res, next) => {
  const { query } = req;
  getDetail(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取用户详情失败",
        status: 1,
      });
      next(err);
    });
});
router.get("/api/user/getInfo", (req, res, next) => {
  const { query } = req;
  getInfo(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取用户信息失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/user/add", (req, res, next) => {
  const { body } = req;
  add(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "添加用户失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/user/update", (req, res, next) => {
  const { body } = req;
  update(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "修改用户失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/user/delete", (req, res, next) => {
  const { body } = req;
  remove(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "删除用户失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/user/batchRemove", (req, res, next) => {
  const { body } = req;
  batchRemove(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "删除用户失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/user/updatePassword", (req, res, next) => {
  const { body } = req;
  updatePassword(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "修改用户失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/user/resetPassword", (req, res, next) => {
  const { body } = req;
  resetPassword(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "重置用户密码失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/user/enabled", (req, res, next) => {
  const { body } = req;
  console.log("enabled-body", body);
  enabled(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "启用用户失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/user/disable", (req, res, next) => {
  const { body } = req;
  disable(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "禁用用户失败",
        status: 1,
      });
      next(err);
    });
});
module.exports = router;
