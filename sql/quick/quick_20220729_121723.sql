-- MySQL dump 10.13  Distrib 8.0.24, for Linux (x86_64)
--
-- Host: localhost    Database: quick
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `per_role_menus`
--

DROP TABLE IF EXISTS `per_role_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `per_role_menus` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int NOT NULL COMMENT '角色编号',
  `menu_id` int NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `per_role_menus`
--

LOCK TABLES `per_role_menus` WRITE;
/*!40000 ALTER TABLE `per_role_menus` DISABLE KEYS */;
INSERT INTO `per_role_menus` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,1,9),(10,1,10),(11,1,11),(12,1,12),(13,1,13),(14,2,1),(15,2,2);
/*!40000 ALTER TABLE `per_role_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `per_user_roles`
--

DROP TABLE IF EXISTS `per_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `per_user_roles` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int NOT NULL COMMENT '用户编号',
  `role_id` int NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `per_user_roles`
--

LOCK TABLES `per_user_roles` WRITE;
/*!40000 ALTER TABLE `per_user_roles` DISABLE KEYS */;
INSERT INTO `per_user_roles` VALUES (1,11,1),(2,45,2);
/*!40000 ALTER TABLE `per_user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_buttons`
--

DROP TABLE IF EXISTS `sys_buttons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_buttons` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `button_id` int NOT NULL COMMENT '按钮编号',
  `button_name` int NOT NULL COMMENT '按钮名称',
  `menu_id` int NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_buttons`
--

LOCK TABLES `sys_buttons` WRITE;
/*!40000 ALTER TABLE `sys_buttons` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_buttons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dictionaries`
--

DROP TABLE IF EXISTS `sys_dictionaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dictionaries` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `dic_type_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `dic_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典编号',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dictionaries`
--

LOCK TABLES `sys_dictionaries` WRITE;
/*!40000 ALTER TABLE `sys_dictionaries` DISABLE KEYS */;
INSERT INTO `sys_dictionaries` VALUES (1,'gender','1','男'),(2,'gender','2','女'),(3,'keyword','3','有偿'),(4,'keyword','4','付费'),(5,'keyword','4','帮忙'),(6,'keyword','5','花钱'),(7,'keyword','6','掏钱'),(8,'keyword','7','有接单的'),(9,'keyword','8','谁接单'),(10,'keyword','9','人接单'),(11,'keyword','10','解决问题'),(12,'keyword','11','求助'),(13,'keyword','12','帮助'),(14,'keyword','13','帮忙'),(15,'keyword','14','联系我'),(17,'keyword','15','有可以做的技术');
/*!40000 ALTER TABLE `sys_dictionaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dictionaries_type`
--

DROP TABLE IF EXISTS `sys_dictionaries_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dictionaries_type` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `dic_type_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型编码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dictionaries_type`
--

LOCK TABLES `sys_dictionaries_type` WRITE;
/*!40000 ALTER TABLE `sys_dictionaries_type` DISABLE KEYS */;
INSERT INTO `sys_dictionaries_type` VALUES (1,'gender','性别'),(2,'keyword','关键字');
/*!40000 ALTER TABLE `sys_dictionaries_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menus`
--

DROP TABLE IF EXISTS `sys_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menus` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单编号',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `path` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路由',
  `menu_type` int NOT NULL COMMENT '菜单类型：0：目录，1：菜单，2：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
  `sort` int NOT NULL COMMENT '排序',
  `btns` json DEFAULT NULL COMMENT '按钮',
  `pid` int NOT NULL COMMENT '父id',
  `link` int NOT NULL COMMENT '是否外链：0：否，1：是',
  `enabled` int NOT NULL COMMENT '是否启用：0：启用，1：禁用',
  `status` int NOT NULL COMMENT '是否显示：0：显示，1：隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menus`
--

LOCK TABLES `sys_menus` WRITE;
/*!40000 ALTER TABLE `sys_menus` DISABLE KEYS */;
INSERT INTO `sys_menus` VALUES (1,'systemManager','系统管理','/system',0,NULL,1,NULL,0,0,0,0),(2,'userManager','用户管理','user',1,NULL,2,NULL,1,0,0,0),(3,'roleManager','角色管理','role',1,NULL,3,NULL,1,0,0,0),(4,'menuManager','菜单管理','menu',1,NULL,4,NULL,1,0,0,0),(5,'deptManager','部门管理','ept',1,NULL,5,NULL,1,0,0,0),(6,'6dictionaryType','字典分类','dictionaryType\n\n',1,NULL,6,NULL,1,0,0,0),(7,'dictionaryManager','字典管理','dictionary',1,NULL,7,NULL,1,0,0,0),(8,'userAdd','新增',NULL,2,NULL,1,NULL,2,0,0,0),(9,'userDel','删除',NULL,2,NULL,2,NULL,2,0,0,0),(10,'userEdit','编辑',NULL,2,NULL,3,NULL,2,0,0,0),(11,'roleAdd','新增',NULL,2,NULL,1,NULL,3,0,0,0),(12,'roleDel','删除',NULL,2,NULL,2,NULL,3,0,0,0),(13,'roleEdit','编辑',NULL,2,NULL,3,NULL,3,0,0,0);
/*!40000 ALTER TABLE `sys_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_roles`
--

DROP TABLE IF EXISTS `sys_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_roles` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编号',
  `role_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_roles`
--

LOCK TABLES `sys_roles` WRITE;
/*!40000 ALTER TABLE `sys_roles` DISABLE KEYS */;
INSERT INTO `sys_roles` VALUES (1,'admin','管理员'),(2,'user','普通用户'),(3,'test','测试专员');
/*!40000 ALTER TABLE `sys_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_users`
--

DROP TABLE IF EXISTS `sys_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_users` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户编号',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `avatar` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `full_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地址',
  `create_time` timestamp NOT NULL COMMENT '创建时间',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '备注',
  `deleted` int NOT NULL COMMENT '是否删除：0：未删除，1：已删除',
  `enabled` int NOT NULL COMMENT '是否启用：0：启用，1：禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_users`
--

LOCK TABLES `sys_users` WRITE;
/*!40000 ALTER TABLE `sys_users` DISABLE KEYS */;
INSERT INTO `sys_users` VALUES (11,'YH_1','admin','14e1b600b1fd579f47433b88e8d85291','','管理员','15229380174','quick@163.com','北京朝阳','2022-05-13 08:20:47','管理员账号，请勿删除',0,1),(45,'YH_2','zhanglp','14e1b600b1fd579f47433b88e8d85291',NULL,'张立平',NULL,NULL,NULL,'2022-07-13 21:05:57',NULL,0,0),(49,'YH_01','test1','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:47:35',NULL,0,0),(50,'YH_02','test2','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:48:06',NULL,0,0),(51,'YH_03','test3','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:48:14',NULL,0,0),(52,'YH_04','test4','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:48:23',NULL,0,0),(53,'YH_05','test5','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:48:29',NULL,0,0),(54,'YH_06','test6','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:48:37',NULL,0,0),(55,'YH_07','test7','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:48:48',NULL,0,0),(56,'YH_08','test8','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:48:56',NULL,0,0),(57,'YH_09','test9','14e1b600b1fd579f47433b88e8d85291',NULL,NULL,NULL,NULL,NULL,'2022-07-19 01:49:04','111',0,0),(61,'YH_001','zhang123','14e1b600b1fd579f47433b88e8d85291',NULL,'张三',NULL,NULL,NULL,'2022-07-19 09:46:15','',0,0);
/*!40000 ALTER TABLE `sys_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'quick'
--

--
-- Dumping routines for database 'quick'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-29 12:17:23
