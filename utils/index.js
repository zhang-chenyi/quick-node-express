const os = require("os");
const moment = require("moment");
const md5 = require("js-md5");

const orderNumber = () => {
  const now = new Date();
  let month = now.getMonth() + 1;
  let day = now.getDate();
  let hour = now.getHours();
  let minutes = now.getMinutes();
  let seconds = now.getSeconds();
  return (
    now.getFullYear().toString() +
    month.toString() +
    day +
    hour +
    minutes +
    seconds +
    Math.round(Math.random() * 89 + 100).toString()
  );
};

const getDate = (date = new Date(), format = "YYYY-MM-DD HH:mm:ss") => {
  return moment(date).format(format);
};

const formatDate = (timeStamp, format = "YYYY-MM-DD HH:mm:ss") => {
  //timeStamp=1659542400  转换后 2022-08-04 00:00:00
  return moment(timeStamp * 1000).format(format); //s
};

const getTimeStamp = () => {
  return moment().format("x");
};
const getIPAddress = () => {
  const interfaces = os.networkInterfaces();
  for (const devName in interfaces) {
    const iface = interfaces[devName];
    for (let i = 0; i < iface.length; i++) {
      const alias = iface[i];
      if (
        alias.family === "IPv4" &&
        alias.address !== "127.0.0.1" &&
        !alias.internal
      ) {
        return alias.address;
      }
    }
  }
};
const quickMd5 = (str) => {
  return md5(str);
};

module.exports = {
  orderNumber,
  getDate,
  formatDate,
  getTimeStamp,
  getIPAddress,
  quickMd5,
};
