const { success } = require("../utils/jsonResult");
const {
  selectWhere,
  insert,
  update,
  remove,
} = require("../model/dictionaryModel");

class DictionaryServices {
  constructor() {}
  async getDictionaryList(dicTypeId) {
    try {
      const dictionaryListResult = await selectWhere(dicTypeId);
      const dictionaryList =
        dictionaryListResult.results && dictionaryListResult.results;
      return success( "字典列表",dictionaryList);
    } catch (error) {
      throw error;
    }
  }
  async addDictionary(dictionary) {
    try {
      await insert(dictionary);
      return success("添加字典成功");
    } catch (error) {
      throw error;
    }
  }
  async updateDictionary(dictionary) {
    try {
      await update(dictionary);
      return success("修改字典成功");
    } catch (error) {
      throw error;
    }
  }
  async deleteDictionary(id) {
    try {
      await remove(id);
      return success("删除字典成功");
    } catch (error) {
      throw error;
    }
  }
}
module.exports = new DictionaryServices();
