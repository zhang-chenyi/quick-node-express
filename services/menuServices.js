const { success } = require("../utils/jsonResult");
const {
  select,
  selectWhere,
  selectOne,
  insert,
  update,
  remove,
} = require("../model/menuModel");

class MenuServices {
  constructor() {}
  async getMenuListByIds(menuIds) {
    try {
      const menuListResult = await selectWhere(menuIds);
      const menuList = menuListResult.results && menuListResult.results;
      return success("授权菜单列表",menuList);
    } catch (error) {
      throw error;
    }
  }
  async getMenuList() {
    try {
      const menuListResult = await select();
      const menuList = menuListResult.results && menuListResult.results;
      return success("授权菜单列表",menuList);
    } catch (error) {
      throw error;
    }
  }
  async getMenuDetail(id) {
    try {
      const menuResult = await selectOne(id);
      const menuDetail = menuResult.results && menuResult.results[0];
      return success("菜单详情",menuDetail);
    } catch (error) {
      throw error;
    }
  }
  async addMenu(menu) {
    try {
      if(!menu.pid){
        menu.pid=0
      }
      await insert(menu);
      return success("添加菜单成功");
    } catch (error) {
      throw error;
    }
  }
  async updateMenu(menu) {
    try {
      await update(menu);
      return success("修改菜单成功");
    } catch (error) {
      throw error;
    }
  }
  async deleteMenu(id) {
    try {
      await remove(id);
      return success("删除菜单成功");
    } catch (error) {
      throw error;
    }
  }
}
module.exports = new MenuServices();
