const { success } = require("../utils/jsonResult");
const { selectWhere, removeWhere, insert } = require("../model/userRoleModel");

class UserRoleServices {
  constructor() {}
  async getUserRoleListByRoleId(roleId) {
    try {
      const userRoleListResult = await selectWhere("role_id", roleId);
      const userRoleList =
        userRoleListResult.results && userRoleListResult.results;
      return success("分配的用户列表", userRoleList);
    } catch (error) {
      throw error;
    }
  }
  async getUserRoleListByUserId(userId) {
    try {
      const userRoleListResult = await selectWhere("user_id", userId);
      const userRoleList =
        userRoleListResult.results && userRoleListResult.results;
      return success("分配的用户列表", userRoleList);
    } catch (error) {
      throw error;
    }
  }
  // async getAssignedUserList(userId) {
  //   try {
  //     const userRoleListResult = await selectWhere("user_id", userId);
  //     const userRoleList =
  //       userRoleListResult.results && userRoleListResult.results;
  //     return success("分配的用户列表", userRoleList);
  //   } catch (error) {
  //     throw error;
  //   }
  // }
  async bindUserForRole(roleId, userIds) {
    try {
      await removeWhere(roleId);
      const userIdArr = userIds.split(",");
      userIdArr.forEach(async (userId) => {
        const userRole = { roleId, userId };
        await insert(userRole);
      });
      return success("角色用户分配成功");
    } catch (error) {
      throw error;
    }
  }
}
module.exports = new UserRoleServices();
