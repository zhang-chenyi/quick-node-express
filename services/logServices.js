const { getDate } = require("../utils/index");
const { success } = require("../utils/jsonResult");
const {
  selectTotal,
  selectPage,
  insert,
  remove,
} = require("../model/logModel");

class LogServices {
  constructor() {}
  async getLogPageList(
    logType,
    current,
    size,
    startTime,
    endTime
  ) {
    try {
      let pageIndex = 1;
      let pageSize = 10;
      if (current < 1) {
        current = 1;
      }
      if (size < 1) {
        size = 10;
      }
      current = parseInt(current);
      size = parseInt(size);
      pageIndex = size * (current - 1);
      pageSize = size;
      const pageListResult = await selectPage(
        logType,
        pageIndex,
        pageSize,
        startTime,
        endTime
      );
      const pageList = pageListResult.results;
      const totalResult = await selectTotal(
        logType,
        startTime,
        endTime
      );
      console.log(totalResult);
      const total = totalResult.results && totalResult.results[0].total;
      return success("日志分页列表", pageList, {
        current,
        size,
        total,
      });
    } catch (error) {
      throw error;
    }
  }
  async addOperateLog(log) {
    try {
      log.logTime = getDate();
      log.logType = 0;
      await insert(log);
      return success(`添加操作日志成功`);
    } catch (error) {
      throw error;
    }
  }
  async addErrorLog(log) {
    try {
      log.logTime = getDate();
      log.logType = 1;
      await insert(log);
      return success(`添加错误日志成功`);
    } catch (error) {
      throw error;
    }
  }
  async removeLog(id) {
    try {
      await remove(id);
      return success("删除日志成功");
    } catch (error) {
      throw error;
    }
  }
  async batchRemoveLog(ids) {
    try {
      await remove(ids);
      return success("删除日志成功");
    } catch (error) {
      throw error;
    }
  }
}
module.exports = new LogServices();
