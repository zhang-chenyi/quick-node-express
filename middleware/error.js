const errorHandler=(err,req,res,next)=>{
    let code=500
    let message='Internal Server Error'
    if(err.name==='UnauthorizedError'){
        code=401
        message='Unauthorized'
    }
    res.statusCode=code;
    res.send({
        status:1,
        msg:'服务器内部错误',
        data:null
    })
}
module.exports=errorHandler