const log4js = require("log4js");
const logConfig = require("../config/log.config");
const { addOperateLog, addErrorLog } = require("../services/logServices");

log4js.configure(logConfig);

module.exports = {
  logOperateHandler: (req, res, next) => {
    console.log('res',res);
    const logger = log4js.getLogger("info");
    const log = {
      operateApi: req.url,
      requestParams: JSON.stringify({ query: req.query, body: req.body }),
      ip: req.ip,
    };
    // logger.info(log);
    if(!req.url.startsWith('/api/log/getPageList')){
      addOperateLog(log);
    }
    next();
  },
  logErrorHandler: (err, req, res, next) => {
    const logger = log4js.getLogger("error");
    const log = {
      operateApi: req.url,
      requestParams: JSON.stringify({ query: req.query, body: req.body }),
      errorMessage: err.message,
      exceptionMessage:err.stack,
      ip: req.ip,
    };
    logger.error(log);
    addErrorLog(log);
    next(err);
  },
};
